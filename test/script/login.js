const assert = require('assert');
const actionObj = require("../../source/actions/actions.js");
const input = require('../input/login.js')
const output = require('../output/login.js')
const app = [];
const actionIns = new actionObj(app);
it('should return true', () => {
    assert.equal(true, true);
});

it('should check valid email and password', () => {
    const res = actionIns.login(input.case1);
    // console.log(res);
    assert.equal(output.case1.message, res.data.message);
});

it('should check invalid email', () => {
    const res = actionIns.login(input.case2);
    // console.log(res);
    assert.equal(output.case2.message, res.data.message);
});

it('should check invalid password', () => {
    const res = actionIns.login(input.case3);
    // console.log(res);
    assert.equal(output.case3.message, res.data.message);
});

it('should check invalid input paramater', () => {
    const res = actionIns.login(input.case4.a);
    // console.log(res);
    assert.equal(output.case4.message, res.data.message);
});
